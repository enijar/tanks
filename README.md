![Logo](https://i.imgur.com/3FgVO7D.png)

### Introduction

This is a 2D game engine for Tanks. The objective of the game is to defeat the enemy AI and players by defending the 
guarded item in your base. You lose if an enemy or AI enters your base and collects the item.

### How to Contribute

You want to contribute? That's awesome! Have a look through the TODO list below, get to work and once you're happy with 
what you have done submit a PR to us.

### TODO

* Randomly generated game map
* Player controls: moving, shooting, etc.
* Enemy AI
* UI: score, time, weapon selection, objectives, etc.

### Contributors

James Craig

Gergely Vazvari
