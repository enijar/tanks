class Map {
    /**
     * Set up the class with the given props.
     * @param {Object} props
     */
    constructor(props) {
        Object.assign(this, props);

        // Each object has a type and a function to render itself to
        // the canvas.
        this.objectTypes = {
            wall: (x, y, width, height) => {
                this.context.fillStyle = '#fff';
                this.context.fillRect(x, y, width, height);
            }
        };

        this.objects = [];

        for (let id = 0; id < 9; id++) {
            this.addObject(id);
        }

        props.Event.on('click', (event) => this.handleClick(event));
    }

    /**
     * Add an object to the objects array with the given id.
     * @param {Number} id
     */
    addObject(id) {
        let object = {
            id: id,
            x: rand(0, this.map.width - 10),
            y: rand(0, this.map.height - 10),
            width: 10,
            height: 10,
            type: 'wall'
        };

        this.objects.push(object);
    }

    /**
     * Remove an object from the objects array with the given id.
     * @param {Number} id
     */
    removeObject(id) {
        this.objects = this.objects.filter((object) => object.id !== id);
    }

    /**
     * Handle click events.
     * @param {Object} event
     */
    handleClick(event) {
        this.objects.map((object) => {
            // Checks if the click event's x, y coordinates are within
            // the object's bounds.
            if (
                event.x >= object.x &&
                event.x <= object.x + object.width &&
                event.y >= object.y &&
                event.y <= object.y + object.height
            ) {
                this.removeObject(object.id);
            }
        });
    }

    /**
     * Render objects to the canvas.
     */
    render() {
        this.context.fillStyle = this.map.backgroundColor;
        this.context.fillRect(0, 0, this.map.width, this.map.height);

        this.objects.map((object) => {
            if (this.objectTypes.hasOwnProperty(object.type)) {
                this.objectTypes[object.type](object.x, object.y, object.width, object.height);
            }
        });
    }
}
